package main

import "github.com/gofiber/fiber"

func main() {
	app := fiber.New()

	app.Get("/", func(c *fiber.Ctx) {
		c.Send("😂 Hello, World! 🌍")
	})

	app.Get("/yo", func(c *fiber.Ctx) {
		c.Send("✋ yo!")
	})

	app.Get("/hey", func(c *fiber.Ctx) {
		c.Send("😉 hey!")
	})

	app.Listen(8080)
}
